#!/bin/bash
git clone https://bitbucket.org/mdelcarmenp/backend-app.git ../backend-app
git clone https://bitbucket.org/mdelcarmenp/ui-app.git ../UI-app

docker-compose up -d

docker-compose ps

# UI-app escucha en el puerto 80
# backend-app escucha en el puerto 4001

