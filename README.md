Dockerización de la app de practitioner bootcamp

Bajar el repositorio docker-compose del proyecto


En el archivo .env se encuentra el acceso a la base de mongo, que debe coincidir con la de la aplicacion backend-app

Ejecutar los comandos:

    git clone https://mdelcarmenp@bitbucket.org/mdelcarmenp/docker-compose.git 
    cd docker-compose
    sh ./install.sh
